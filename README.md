# CFForm to AngularJS

CFForm is a simple directive of builder form interface to [formly](http://docs.angular-formly.com).

## It is assumed
- Install nodejs: `sudo apt-get install nodejs npm`
- Install bower and update: `sudo npm install -g bower` and `bower update`

## Usage
- With bower: `bower install --save cfform`
- Add **cf** as a dependency to your app. Example:
```
angular.module('project', [
    ...,
    'cf'
])
```

## In html:

```
<script src="./bower_components/cf-form/dist/cf-form-builder.min.js"></script>
```