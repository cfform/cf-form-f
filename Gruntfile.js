module.exports = function (grunt) {
  /*
   * Project configuration.
   */
  grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      mkdir: {
        build: {
          options: {
            create: ['build/<%= pkg.name %>'],
            mode  : '0775'
          }
        }
      },

      concat: {
        backend: {
          src : [
            'dist/**/*.js',
            '!dist/**/*.min.js'
          ],
          dest: 'dist/cf-form-builder.min.js'
        }
      },

      cssmin: {
        all: {
          files: [{
            expand: true,
            cwd   : 'dist/',
            src   : ['*.css', '!*.min.css'],
            dest  : 'dist/',
            ext   : '.min.css'
          }]
        }
      },

      uglify: {
        backend: {
          options: {
            sourceMap              : true,
            sourceMapIncludeSources: true
          },
          files  : {
            'dist/cf-form-builder.min.js': ['dist/cf-form-builder.min.js']
          }
        }
      },

      copy: {
        release: {
          files: [
            {
              src : [
                '**',
                '!node_modules/**',
                '!Gruntfile*',
                '!.*',
                '!package.json',
                '!bower.json',
                '!gulpfile.js',
                '!.bowerrc',
                '!.gitignore',

                '!dist/**/*.{css,js,ts,d.ts,js.map}',
                'dist/**/*.min.css',
                'dist/**/*.min.js',
                'dist/**/*.min.js.map',

                'dist/**/*.html',
                'example/**/*',
                //'!example/bower_components/**',
              ],
              dest: '<%= mkdir.build.options.create[0] %>/'
            }
          ]
        },
        dev    : {
          files: [
            {
              src : [
                '**',
                //'!node_modules/**',
                //'!bower_components/**',
                //'!.*'
              ],
              dest: '<%= mkdir.build.options.create[0] %>/'
            }
          ]
        }
      },

      compress: {
        build: {
          options: {
            archive: '<%= pkg.name %>.zip',
            level  : 6
          }
          ,
          expand : true,
          cwd    : '<%= mkdir.build.options.create[0] %>/',
          src    : ['**/*'],
          dest   : ''
        }
      }
      ,

      clean: {
        build: ['build/']
      },

      watch: {
        scripts: {
          files  : [
            'dist/**/*.js',
            'example/app/views/**/*.html'
          ],
          tasks  : [
            'app'
          ],
          options: {
            spawn: false
          }
        }
      },

      browserSync: {
        default_options: {
          bsFiles: {
            src: [
              'dist/**/*.js',
              'example/**/*.css',
              'example/**/*.html'
            ]
          },
          options: {
            watchTask: true,
            server   : {
              baseDir: "./"
              //directory: true
            }
          }
        }
      }

    }
  );

  /*
   * Load plugins.
   */
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-browser-sync');
  //grunt.loadNpmTasks('grunt-contrib-connect');

  /*
   * Custom tasks.
   */

  /**
   * Task: Generate .zip with version of project to production.
   */
  grunt.registerTask('release', [
    'mkdir:build',
    'copy:release',
    'compress:build',
    'clean:build'
  ]);

  /**
   * Task: Generate .zip with all files of project.
   */
  grunt.registerTask('dev', [
    'mkdir:build',
    'copy:dev',
    'compress:build',
    'clean:build'
  ]);

  /**
   * Task: App.
   */
  grunt.registerTask('app', [
    //'copy:backend',
    'concat:backend',
    'cssmin:all',
    //'connect:server'
    //'uglify:backend'
  ]);

  grunt.registerTask('server', ['browserSync', 'watch']);

  /*
   * Default tasks.
   */
  grunt.registerTask('default', ['release']);
};
