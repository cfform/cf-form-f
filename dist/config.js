/**
 * Config file.
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', function ($rootScope) {
    var config = {};

    config.rootElements = [
      {
        element: 'all',
        title  : 'All'
      },
      {
        element: 'input',
        title  : 'Input'
      },
      {
        element: 'select',
        title  : 'Select'
      },
      {
        element: 'datepicker',
        title  : 'Date Picker'
      },
      {
        element: 'richtext',
        title  : 'Rich Text'
      },
      {
        element: 'cffield',
        title  : 'CF'
      }
    ];

    $rootScope.$emit('cf:form:root:elements', config);
  }]);