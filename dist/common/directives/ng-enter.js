  /**
 * Module Directives specific to app.
 */
'use strict';

angular.module("cfFormDirectives")

/**
 * ngEnter directive if you can use submit form(https://twitter.com/ririlepanda)
 *
 * Example:
 * <input type="text" ng-model="item" ng-enter="create(item)">
 */
  .directive('ngEnter', function () {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if (event.which === 13) {
          scope.$apply(function () {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      });
    }
  })