/**
 * Module Directives specific to app.
 */
angular.module("cf")

/**
 * Directive to builder forms interface to formly.
 * Example:
 */
  .directive('cfform', [function () {
    return {
      restrict: 'A',
      replace : true,
      require : '?ngModel',

      scope: {
        model: '=ngModel'
      },

      templateUrl: '.././dist/cf-form.html',

      link: ['scope', 'element', function (scope, element) {
        var $this = element;

        // Set active class when click on some element.
        $this.on("click", ".cf-elements li", function () {
          jQuery('.cf-elements li').removeClass('active');
          jQuery(this).addClass('active');
        });

        //$this.on("click", ".cf-create-object", function () {
        //  jQuery('.cf-elements li').removeClass('active');
        //});
      }],

      controller: ['$scope', '$rootScope', 'formlyVersion', 'CfBuilder', function ($scope, $rootScope, formlyVersion, CfBuilder) {
        $scope.rootElements = $rootScope.rootElements;
        $scope.extras = $rootScope.extras;

        $scope.vm = {}; // vm stands for "View Model" --> see https://github.com/johnpapa/angular-styleguide#controlleras-with-vm
        $scope.vm.model = {};
        $scope.element = {};

        $scope.tmp = {};
        $scope.tmp.modelTmp = {}; // Temporal model to preview and not used.

        // Data of libraries.
        $scope.vm.env = {
          angularVersion: angular.version.full,
          formlyVersion : formlyVersion
        };

        $scope.showCreateForm = false;

        // TODO: VCA: Cargar esta variable dependiendo de si el usuario me pasa lista de elementos propios.
        //$scope.elements = elements;

        // Get initial elements to show from root elements ($rootScope.rootElements) in position 0.
        $scope.rootElementItem = $rootScope.rootElements[0];
        $scope.elements = _.where($rootScope.elements, {parent: $scope.rootElementItem.element});
        $scope.vm.modelFields = $scope.elements[0].modelFields;
        $scope.element = $scope.elements[0];

        // Select a root element.
        $scope.selectRootElement = function (rootElement) {
          $scope.elements = _.where($rootScope.elements, {parent: rootElement.element});
          $scope.vm.modelFields = [];
        };

        // Show form to create object.
        $scope.createNewObject = function () {
          if ($scope.showCreateForm) {
            $scope.showCreateForm = false;
          } else {
            $scope.showCreateForm = true;
          }
        };

        // Select a element.
        $scope.selectElement = function (element) {
          $scope.vm.modelFields = [];
          CfBuilder.selectElement(element, function (result, error) {
            if (is.not.empty(result)) {
              $scope.vm.modelFields = result.modelFields;
            } else if (is.not.empty(error)) {
              $scope.msg = error;
            }
          });
        };

        //-------------------------------------------------------------------------

        $scope.vm.saveField = function () {
          $scope.msg = '';
          CfBuilder.saveField($scope.element, $scope.vm.model, $scope.model, function (result, error) {
            if (is.not.empty(result)) {
              $scope.model.push(result.newField);
              $scope.vm.model = result.model;
            } else if (is.not.empty(error)) {
              $scope.msg = error;
            }
          });
        };

        //-------------------------------------------------------------------------

        $scope.vm.editField = function (model, changeModelScheme) {
          CfBuilder.editField($scope.model, $rootScope.elements, model, changeModelScheme, function (result, error) {
            if (is.not.empty(result)) {
              $scope.model[result.index] = result.editField;
            }
          });
        };

        //-------------------------------------------------------------------------

        $scope.vm.deleteField = function (model) {
          angular.forEach($scope.model, function (value, key) {
            if (value.data.id == model.data.id) {
              $scope.model.splice(key, 1);
            }
          });
        };

        //-------------------------------------------------------------------------

        $scope.vm.upField = function (model, index) {
          if (index != 0) {
            var before = $scope.model[index - 1];
            $scope.model[index - 1] = model;
            $scope.model[index] = before;
          }
        };

        //-------------------------------------------------------------------------

        $scope.vm.downField = function (model, index) {
          if (!!$scope.model[index + 1]) {
            var after = $scope.model[index + 1];
            $scope.model[index + 1] = model;
            $scope.model[index] = after;
          }
        };
      }]
    }
  }])