/**
 * Module Directives specific to app.
 */
angular.module("cf")

/**
 * Directive to dragdrop.
 * Example:
 *
 * <div cfformdrag ng-model="formModel" concepts="concepts"></div>
 *
 * Example of one form:
 *
 * {
        "data_form": {                      // Json. Data from cfformdrag directive for concept build.
          "options": {},
          "rootList": [],
          "model": []
        },
        "name": "First Form",               // Name of form.
        "children": [],                     // Children. This array is always empty and only instance in the tree of the version control.
        "id": 1444672909934,                // ID of form.
        "parent": 1444672863831,            // ID from parent of the form. When 0 it means that the form doesn't have parent.
        "is_new_version": true              // Is this a new version of the form?. When false it means that the form be not shown
                                               in the list of versions because the content inherited of the father was not modified,
                                               it therefore is not a new version.
   }
 *
 * The directive accepts the following attributes:
 *
 * - 'ng-model':    Scope model.
 * - 'concepts':    List of concepts. Example of the one concept in the list:
 * {
        "data_concept": {                   // Json. Data from cfconceptdrag directive for concept build.
          "options": {
            "change": 1444673517354.3542
          },
          "rootList": [],
          "model": []
        },
        "name": "First Concept",            // Name of concept.
        "children": [],                     // Children. This array is always empty and only instance in the tree of the version control.
        "id": 1444672909934,                // ID of concept.
        "parent": 1444672863831,            // ID from parent of the concept. When 0 it means that the concept doesn't have parent.
        "is_new_version": true              // Is this a new version of the concept?. When false it means that the concept be not shown
                                               in the list of versions because the content inherited of the father was not modified,
                                               it therefore is not a new version.
   }
 */
  .directive('cfformdrag', function () {
    var strTemplate = "";
    strTemplate += "<div class=\"row\">";
    strTemplate += "  <div>";
    strTemplate += "";
    strTemplate += "    <!--<pre>{{ elements[0] | json }}<\/pre>-->";
    strTemplate += "";
    strTemplate += "    <div class=\"cf-drag-tab cf-drag-form\">";
    strTemplate += "      <!-- Tab panes -->";
    strTemplate += "      <div class=\"tab-content\">";
    strTemplate += "        <div ng-if=\"!elements || elements.length == 0\" class=\"alert\" style=\"margin-bottom: 0px;\">";
    strTemplate += "          <translate>Concepts don't exist, you should create concepts in order to could create forms<\/translate>";
    strTemplate += "        <\/div>";
    strTemplate += "";
    strTemplate += "        <div id=\"containerConcepts\">";
    strTemplate += "          <li type=\"button\"";
    strTemplate += "              class=\"btn btn-default btn-sm\"";
    strTemplate += "              ng-class=\"{'cf-preview-concept-form': order}\"";
    strTemplate += "              ng-repeat=\"element in elements track by $index\"";
    strTemplate += "              ng-click=\"order = order ? false : true\"";
    strTemplate += "              ng-show=\"element.name\">";
    strTemplate += "            <i class=\"glyphicon glyphicon-folder-close\"";
    strTemplate += "               ng-init=\"order = false\"";
    strTemplate += "               ng-class=\"{'cf-preview-concept-icon-close glyphicon-folder-open': order, 'glyphicon-folder-close': !order}\"";
    strTemplate += "               style=\"font-size: 11px;margin-right: 1px;\"><\/i>";
    strTemplate += "            {{element.name}}";
    strTemplate += "";
    strTemplate += "            <div ng-show=\"order\">";
    strTemplate += "              <div class=\"cf-drag-well only-read\" ng-if=\"element.data_concept.model\">";
    strTemplate += "                <formly-form fields=\"element.data_concept.model\"><\/formly-form>";
    strTemplate += "              <\/div>";
    strTemplate += "            <\/div>";
    strTemplate += "          <\/li>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "";
    strTemplate += "  <div class=\"cf-drag-preview\"";
    strTemplate += "       ng-class=\"{'active': newChange, 'preview': preview}\"";
    strTemplate += "       ng-init=\"preview = false\"";
    strTemplate += "       ng-click=\"preview = preview ? false : true\"";
    strTemplate += "       title=\"Preview\"><\/div>";
    strTemplate += "";
    strTemplate += "  <div class=\"col-md-12 cf-dropzone cf-form\">";
    strTemplate += "";
    strTemplate += "    <div ng-show=\"preview\" class=\"row cf-drag-preview-show\">";
    strTemplate += "      <div class=\"column-preview col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
    strTemplate += "        <div class=\"page-header\" style=\"margin: 12px 0 19px;padding-bottom: 6px;\">";
    strTemplate += "          <h4>";
    strTemplate += "            <translate>Preview<\/translate>";
    strTemplate += "            <small>";
    strTemplate += "              <translate>Show form generated from the concepts.<\/translate>";
    strTemplate += "            <\/small>";
    strTemplate += "          <\/h4>";
    strTemplate += "        <\/div>";
    strTemplate += "";
    strTemplate += "        <!--<cfform-show-concept ng-model=\"modelShow\" concepts=\"model.model\"><\/cfform-show-concept>-->";
    strTemplate += "        <cfform-show ng-model=\"modelShow\" fields=\"model.model\"><\/cfform-show>";
    strTemplate += "      <\/div>";
    strTemplate += "      <div ng-show=\"modelShow\" class=\"column-preview col-xs-12 col-sm-12 col-md-12 col-lg-12\">";
    strTemplate += "        <pre>{{ modelShow | json }}<\/pre>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "";
    strTemplate += "    <div ng-show=\"!preview\" id=\"containerFormRootList\" class=\"row\">";
    strTemplate += "      <div ng-repeat=\"item in model.rootList track by $index\"";
    strTemplate += "           class=\"cf-drag-columns {{ item.classes.text }}\"";
    strTemplate += "           ng-class=\"{'col-md-12': !item.classes.text}\">";
    strTemplate += "";
    strTemplate += "        <div class=\"cf-drag-actions-visible\">";
    strTemplate += "          <div class=\"left-column\">";
    strTemplate += "            <i class=\"increasing\" ng-click=\"increasing(item, $index)\"><\/i>";
    strTemplate += "            <i class=\"decreasing\" ng-click=\"decreasing(item, $index)\"><\/i>";
    strTemplate += "          <\/div>";
    strTemplate += "";
    strTemplate += "          <div class=\"panel panel-default cf-drag-panel\">";
    strTemplate += "            <div class=\"panel-heading\">";
    strTemplate += "              <i class=\"glyphicon glyphicon-move\"><\/i> {{ item.name }}";
    strTemplate += "";
    strTemplate += "              <div class=\"pull-right\">";
    strTemplate += "                <div class=\"cf-widget-toolbar\">";
    strTemplate += "                  <div class=\"cf-drag-actions\" style=\"visibility: hidden;\">";
    strTemplate += "                    <a title=\"Minimize\/Maximize\" data-toggle=\"collapse\"";
    strTemplate += "                       ng-click=\"collapse(item.ft)\"";
    strTemplate += "                       href=\".{{ item.ft }}\" aria-expanded=\"false\"";
    strTemplate += "                       aria-controls=\"{{ item.ft }}\">";
    strTemplate += "                      <i class=\"{{ item.ft }}-toggle icon-action glyphicon glyphicon-chevron-up\"><\/i>";
    strTemplate += "                    <\/a>";
    strTemplate += "";
    strTemplate += "                    <a title=\"Edit Options\" href=\"\" ng-click=\"showEditModal(item, index)\">";
    strTemplate += "                      <i class=\"icon-action green glyphicon glyphicon-pencil\"><\/i>";
    strTemplate += "                    <\/a>";
    strTemplate += "";
    strTemplate += "                    <a title=\"Remove\" href=\"\" ng-init=\"showRemove = false\" ng-click=\"showRemove = true\">";
    strTemplate += "                      <i class=\"icon-action red glyphicon glyphicon-remove\"><\/i>";
    strTemplate += "                    <\/a>";
    strTemplate += "";
    strTemplate += "                    <div ng-show=\"showRemove\" class=\"cf-sure-remove-box\">";
    strTemplate += "                      <translate>You sure remove this?<\/translate>";
    strTemplate += "                      <button type=\"submit\" class=\"btn btn-danger btn-xs\" ng-click=\"remove($index); showRemove = false\">";
    strTemplate += "                        <translate>Yes<\/translate>";
    strTemplate += "                      <\/button>";
    strTemplate += "                      <button type=\"submit\" class=\"btn btn-default btn-xs\" ng-click=\"showRemove = false\">";
    strTemplate += "                        <translate>No<\/translate>";
    strTemplate += "                      <\/button>";
    strTemplate += "                    <\/div>";
    strTemplate += "                  <\/div>";
    strTemplate += "                <\/div>";
    strTemplate += "              <\/div>";
    strTemplate += "            <\/div>";
    strTemplate += "          <\/div>";
    strTemplate += "";
    strTemplate += "          <div class=\"{{ item.ft }} collapse in\" style=\"margin-top: -2px;\">";
    strTemplate += "            <div class=\"well cf-drag-well only-read\" ng-if=\"item.data_concept.model.length > 0\">";
    strTemplate += "              <div class=\"row\">";
    strTemplate += "                <formly-form fields=\"item.data_concept.model\"><\/formly-form>";
    strTemplate += "              <\/div>";
    strTemplate += "            <\/div>";
    strTemplate += "";
    strTemplate += "            <div class=\"well cf-drag-well only-read\" ng-if=\"item.data_concept.model.length == 0\">";
    strTemplate += "              <div class=\"alert alert-without cf-drag-alert\">";
    strTemplate += "                <i class=\"glyphicon glyphicon-exclamation-sign\"><\/i>";
    strTemplate += "                <translate>Concept without model, please edit this and insert data.<\/translate>";
    strTemplate += "              <\/div>";
    strTemplate += "            <\/div>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "";
    strTemplate += "      <div class=\"col-md-12\" style=\"padding: 6px;\"><\/div>";
    strTemplate += "";
    strTemplate += "      <div class=\"col-md-12 cf-dropzone-clear\" ng-hide=\"hide()\" style=\"font-size: 12px;\">";
    strTemplate += "        <div><\/div>";
    strTemplate += "        <i class=\"glyphicon glyphicon-plus\"><\/i>";
    strTemplate += "        <br>";
    strTemplate += "        <translate>Add your items here<\/translate>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "";
    strTemplate += "  <\/div>";
    strTemplate += "";
    strTemplate += "  <!-- Modal -->";
    strTemplate += "  <div class=\"cf-form-drag-modal modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\"";
    strTemplate += "       aria-hidden=\"true\">";
    strTemplate += "    <div class=\"modal-dialog\" style=\"width : 72%;\">";
    strTemplate += "      <div class=\"modal-content\">";
    strTemplate += "        <div class=\"modal-header\">";
    strTemplate += "          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;<\/button>";
    strTemplate += "          <h4 class=\"modal-title\" id=\"modalLabel\">{{ headTitle }}<\/h4>";
    strTemplate += "        <\/div>";
    strTemplate += "        <div class=\"modal-body\">";
    strTemplate += "          <div class=\"row\">";
    strTemplate += "            <div class=\"col-md-12 col-lg-12 col-sm-12 col-xs-12\">";
    strTemplate += "";
    strTemplate += "              <div ng-if=\"element\">";
    strTemplate += "                <div cfconceptdrag ng-model=\"element.data_concept\"><\/div>";
    strTemplate += "              <\/div>";
    strTemplate += "";
    strTemplate += "            <\/div>";
    strTemplate += "          <\/div>";
    strTemplate += "        <\/div>";
    strTemplate += "        <div class=\"modal-footer\">";
    strTemplate += "          <button type=\"button\" class=\"btn btn-primary no-border\" ng-click=\"editThisConcept(element)\">";
    strTemplate += "            <translate>Edit this Concept to only this form<\/translate>";
    strTemplate += "          <\/button>";
    strTemplate += "          <button type=\"button\" class=\"btn btn-default no-border\" data-dismiss=\"modal\">";
    strTemplate += "            <translate>Close<\/translate>";
    strTemplate += "          <\/button>";
    strTemplate += "        <\/div>";
    strTemplate += "      <\/div>";
    strTemplate += "    <\/div>";
    strTemplate += "  <\/div>";
    strTemplate += "";
    strTemplate += "  <p class=\"cf-drag-bottom-data\">";
    strTemplate += "    {{ model.rootList.length }}";
    strTemplate += "    <translate>concepts<\/translate>";
    strTemplate += "";
    strTemplate += "    <span class=\"pull-right\"><translate>Form Dropzone<\/translate><\/span>";
    strTemplate += "  <\/p>";
    strTemplate += "";
    strTemplate += "  <!--<div class=\"col-md-12\">-->";
    strTemplate += "  <!--<pre>{{ model.rootList | json }}<\/pre>-->";
    strTemplate += "  <!--<\/div>-->";
    strTemplate += "<\/div>";

    return {
      restrict: 'A',
      replace : true,
      require : '?ngModel',
      scope   : {
        model   : '=ngModel',
        elements: '=?concepts'
      },

      //templateUrl: '../dist/cf-form-drag.html',
      template: strTemplate,

      link: function (scope, element) {

        scope.collapse = function (id) {
          var $elCollapse = element.find('.' + id);
          setTimeout(function () {
            var $elToggle = element.find('.' + id + '-toggle');
            if ($elCollapse.hasClass('in')) {
              $elToggle.removeClass('glyphicon-chevron-down');
              $elToggle.addClass('glyphicon-chevron-up');
            } else {
              $elToggle.removeClass('glyphicon-chevron-up');
              $elToggle.addClass('glyphicon-chevron-down');
            }
          }, 500);
        };
      },

      controller: ['$scope', '$rootScope', '$element', '$timeout', 'CfBuilder', 'dragularService', 'panels', function ($scope, $rootScope, $element, $timeout, CfBuilder, dragularService, panels) {
        $scope.elements = angular.isDefined($scope.elements) ? $scope.elements : [];

        //$scope.model = {
        //  options : {},
        //  rootList: [],
        //  model   : []
        //};

        //----

        if (angular.isDefined($scope.model)) {
          if (!$scope.model.options || !$scope.model.rootList) {
            $scope.model = {
              options : {},
              rootList: [],
              model   : []
            };
          }
        } else {
          $scope.model = {
            options : {},
            rootList: [],
            model   : []
          };
        }

        $scope.vm = {};

        $scope.hide = function () {
          return $scope.model.rootList.length > 0;
        };

        // Block Start: Dragular drag&drop.
        var containerConcepts = document.querySelector('#containerConcepts');
        var containerFormRootList = document.querySelector('#containerFormRootList');

        self.accepts = function (el, target, source) {
          if (source === containerConcepts || source === target) {
            return true;
          }
        };

        dragularService([containerConcepts], {
          containersModel: [$scope.elements],
          copy           : true,
          //move only from left to right
          accepts        : self.accepts
        });

        dragularService([containerFormRootList], {
          containersModel: [$scope.model.rootList],
          //removeOnSpill  : false,
          //move only from left to right
          accepts        : self.accepts,
          scope          : $scope,
          classes        : {
            mirror      : 'gu-mirror',
            hide        : 'gu-hide',
            unselectable: 'gu-unselectable',
            transit     : 'gu-transit',
            overActive  : 'gu-over-active',
            overAccepts : 'gu-over-accept',
            overDeclines: 'gu-over-decline'
          }
        });
        // Block End: Dragular drag&drop.

        self.onStopForm = function (element, index) {
          //$scope.action = 'add';

          element.ft = new Date().getTime(); // form token (ft)
          $scope.model.rootList[index] = angular.copy(element);
          $scope.model.rootList[index].classes = {
            text: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
            numb: 12
          };
        };

        $scope.$watch('model.rootList', function (newValue, oldValue) {
          if (newValue.length > oldValue.length) {
            angular.forEach(newValue, function (value, index) {
              if (is.not.propertyDefined(value, 'ft')) { // Check if ceoncept have the form token (ft)
                self.onStopForm($scope.model.rootList[index], index);
              }
            });
          }

          $scope.model.model = [];
          angular.forEach($scope.model.rootList, function (value, index) {
            if (is.propertyDefined(value.data_concept, 'model')) {

              // TODO: VCA: Aquí voy a armar el model.model con los modelos de todos los conceptos en un solo array para armar el formulario de una vez.
              // TODO: VCA: Funciona con <cfform-show ng-model="modelShow" fields="model.model"></cfform-show>
              //angular.forEach(value.data_concept.model, function (valueModel) {
              //  $scope.model.model[0].fieldGroup.push(angular.copy(valueModel));
              //});

              // TODO: VCA: Aquí podría ver lo de separar en box diferentes los conceptos diferentes mediante una clase css.
              // TODO: VCA: Funciona con <cfform-show ng-model="modelShow" fields="model.model"></cfform-show>
              var buildModel = [];
              buildModel.push({
                template: '<div class="cf-header-in-template page-header"><strong>' + index + '. ' + value.name + '</strong></div>'
              });

              $scope.model.model.push({
                "className" : "cf-one-concept " + value.classes.text,
                "fieldGroup": buildModel.concat(angular.copy(value.data_concept.model))
              });

              // TODO: VCA: Funciona con <cfform-show-concept ng-model="modelShow" concepts="model.model"></cfform-show-concept>
              //$scope.model.model.push(value);

              $scope.newChange = true;
              $timeout(function () {
                $scope.newChange = false;
              }, 1000);
            }
          });
        }, true);

        /**
         * Show modal with "cfconceptdrag" directive.
         *
         * @param element
         * @param index
         */
        $scope.showEditModal = function (element) {
          $scope.element = angular.copy(element);

          $scope.headTitle = 'Editar Concepto: ' + $scope.element.name;
          $element.find('.cf-form-drag-modal').modal();
        };

        /**
         * Edit data_concept of element and change only in this concept but not in list of concepts.
         *
         * @param element
         */
        $scope.editThisConcept = function (element) {
          angular.forEach($scope.model.rootList, function (value, index) {
            if (element.id == value.id) {
              $scope.model.rootList[index].data_concept = angular.copy(element.data_concept);
              $element.find('.cf-form-drag-modal').modal('hide');
            }
          });
        };

        $scope.remove = function (index) {
          $scope.model.rootList.splice(index, 1);
        };

        $scope.decreasing = function (element, index) {
          $scope.element = angular.copy(element);

          var number = element.classes.numb;
          if (number > 0) {
            number = (number == 1) ? number : number - 1;

            $scope.model.rootList[index].classes = {
              text: 'col-xs-12 ' + 'col-sm-' + (number) + ' col-md-' + (number) + ' col-lg-' + (number),
              numb: number
            };
          }
        };

        $scope.increasing = function (element, index) {
          $scope.element = angular.copy(element);

          var number = element.classes.numb;
          if (number < 12) {
            number = (number == 12) ? number : number + 1;

            $scope.model.rootList[index].classes = {
              text: 'col-xs-12 ' + 'col-sm-' + (number) + ' col-md-' + (number) + ' col-lg-' + (number),
              numb: number
            };
          }
        };

      }]
    }
  });