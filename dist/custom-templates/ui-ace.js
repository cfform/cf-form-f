/**
 * Element: UI Ace.
 * https://github.com/angular-ui/ui-ace
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {
    var element = {
      name          : 'ui-ace',
      //extends       : 'input',
      //wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      template      : '<div ng-model="model[options.key]" ui-ace="optionsAce"></div>',
      defaultOptions: {
        templateOptions: {
          label    : '',
          cfoptions: {
            theme      : 'xcode',
            language   : 'javascript',
            useWrapMode: true,
            showGutter : true
          }
        }
      },
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', function ($scope) {

        $scope.optionsAce = {
          useWrapMode    : $scope.to.cfoptions.useWrapMode,
          showGutter     : $scope.to.cfoptions.showGutter,
          theme          : $scope.to.cfoptions.theme,
          mode           : $scope.to.cfoptions.language,
          firstLineNumber: 1
          //onLoad         : aceLoaded,
          //onChange       : aceChanged
        }

      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }]);