/**
 * Element: jQuery Chosen.
 * Create By: CF.
 */

angular.module("cf")

  .run(['$rootScope', 'formlyConfig', function ($rootScope, formlyConfig) {
    var element = {
      name          : 'chosen',
      //extends       : 'input',
      wrapper       : ['bootstrapLabel', 'bootstrapHasError'],
      template      : '<div ng-if="!to.cfoptions.multiple">' +
      '<select chosen ng-model="model[options.key]" data-placeholder="{{ to.cfoptions.dataPlaceholder }}" no-results-text="to.cfoptions.noResultText" ng-options="s[to.cfoptions.valueFilter] for s in to.list"><option value=""></option></select>' +
      '</div>' +
      '<div ng-if="to.cfoptions.multiple">' +
      '<select multiple chosen ng-model="model[options.key]" data-placeholder="{{ to.cfoptions.dataPlaceholder }}" no-results-text="to.cfoptions.noResultText" ng-options="s[to.cfoptions.valueFilter] for s in to.list"><option value=""></option></select>' +
      '</div>',
      defaultOptions: {
        templateOptions: {
          label    : 'Set the name of the field label',
          cfoptions: {
            multiple       : false,
            dataPlaceholder: 'Choose a value',
            noResultText   : 'Could not find any values',
            valueFilter    : 'name'
          },
          list     : []
        }
      },
      link          : function (scope, el, attrs) {
      },
      controller    : ['$scope', '$rootScope', 'CfBuilder', function ($scope, $rootScope, CfBuilder) {

        // Example: project.Person.getAll
        CfBuilder.executeGetGenericData($scope.to.cfoptions.dataFactory, function (result) {
          $scope.to.list = result;
        });
      }],
      apiCheck      : {}
    };

    formlyConfig.setType(element);
    $rootScope.$emit('cf:form:extra', element);
  }]);